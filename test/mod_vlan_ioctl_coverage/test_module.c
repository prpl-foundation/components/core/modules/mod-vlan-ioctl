/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxm/amxm.h>

#include <linux/sockios.h>

#include "test_module.h"
#include "mod_vlan_ioctl.h"
#include "../common/mock.h"

#define MOD_NAME "target_module"
#define MOD_PATH "../modules_under_test/" MOD_NAME ".so"

int test_setup(UNUSED void** state) {

    return 0;
}

int test_teardown(UNUSED void** state) {

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, MOD_NAME, MOD_PATH), 0);
}

void test_has_functions(UNUSED void** state) {
    assert_true(amxm_has_function(MOD_NAME, MOD_VLAN_CTRL, "create-vlan"));
    assert_true(amxm_has_function(MOD_NAME, MOD_VLAN_CTRL, "destroy-vlan"));
    assert_true(amxm_has_function(MOD_NAME, MOD_VLAN_CTRL, "set-vlan-priority"));
}

void test_create_vlan(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "create-vlan", &var, &ret), -1);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "port_name", "eth0");
    amxc_var_add_key(uint32_t, &var, "egress_priority", 5);
    amxc_var_add_key(int32_t, &var, "vlanid", 20);

    ioctl_should_fail_on(SIOCSIFVLAN);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "create-vlan", &var, &ret), 2);

    ioctl_should_fail_on(DO_NOT_FAIL);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "create-vlan", &var, &ret), 0);
    assert_non_null(GETP_CHAR(&ret, "0"));
    assert_string_equal(GETP_CHAR(&ret, "0"), "eth0.20");

    amxc_var_add_key(cstring_t, &var, "vlanport_name", "vlan20");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "create-vlan", &var, &ret), 0);
    assert_non_null(GETP_CHAR(&ret, "0"));
    assert_string_equal(GETP_CHAR(&ret, "0"), "vlan20");

    ioctl_should_fail_on(SIOCSIFNAME);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "create-vlan", &var, &ret), 3);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_destroy_vlan(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "create-vlan", &var, &ret), -1);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "vlanport_name", "vlan20");

    ioctl_should_fail_on(SIOCGIFFLAGS);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "destroy-vlan", &var, &ret), 0);

    ioctl_should_fail_on(SIOCSIFFLAGS);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "destroy-vlan", &var, &ret), 0);

    ioctl_should_fail_on(SIOCSIFVLAN);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "destroy-vlan", &var, &ret), 0);

    ioctl_should_fail_on(DO_NOT_FAIL);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "destroy-vlan", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_destroy_vlan_on_name_and_vlanid(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "create-vlan", &var, &ret), -1);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "port_name", "eth0");
    amxc_var_add_key(uint32_t, &var, "vlanid", 20);

    ioctl_should_fail_on(DO_NOT_FAIL);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "destroy-vlan", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_set_vlan_priority(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "set-vlan-priority", &var, &ret), -1);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "vlanport_name", "eth0");
    amxc_var_add_key(uint32_t, &var, "egress_priority", 5);

    ioctl_should_fail_on(SIOCSIFVLAN);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "set-vlan-priority", &var, &ret), 2);

    ioctl_should_fail_on(DO_NOT_FAIL);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_VLAN_CTRL, "set-vlan-priority", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_can_close_module(UNUSED void** state) {
    assert_int_equal(amxm_close_all(), 0);
}
