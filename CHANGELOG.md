# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.7 - 2023-11-23(16:29:33 +0000)

### Fixes

- Configure VLANPriority per service on a VLAN

## Release v0.1.6 - 2023-10-20(12:02:35 +0000)

### Fixes

- Can't remove vlan if user friendly name is not set

## Release v0.1.5 - 2023-10-09(06:58:17 +0000)

### Fixes

- Failed to rename Vlan interfaces after a WAN.Reset()

## Release v0.1.4 - 2023-05-11(09:29:14 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v0.1.3 - 2022-05-19(12:47:16 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.1.2 - 2022-04-13(11:50:40 +0000)

### Fixes

- Vlan Interfaces are not up after creation

## Release v0.1.1 - 2021-11-04(11:51:59 +0000)

### Changes

- Move vlan modules from ambiorix folder to core on gitlab.com

## Release v0.1.0 - 2021-10-01(09:12:33 +0000)

### New

- Create a SoC module handling VLANs in ioctl

